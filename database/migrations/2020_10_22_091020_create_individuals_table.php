<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateIndividualsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('individuals', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->integer('index')->nullable();
            $table->integer('phone')->nullable();
            $table->string('nic')->nullable();
            $table->string('name')->nullable();
            $table->string('type')->nullable();
            $table->integer('allowed')->nullable();
            $table->integer('age')->nullable();
            $table->string('address')->nullable();
            $table->string('info')->nullable();
            $table->string('gender')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('individuals');
    }
}
