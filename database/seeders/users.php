<?php

namespace Database\Seeders;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
use Spatie\Permission\PermissionRegistrar;

use Illuminate\Database\Seeder;

class users extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        app()[PermissionRegistrar::class]->forgetCachedPermissions();
        Permission::create(['name' => 'manage users']);        
        Permission::create(['name' => 'manage records']);        
        
        $admin = Role::create(['name' => 'admin']);
        $admin->givePermissionTo('manage users');
        $admin->givePermissionTo('manage records'); 

        $user = Role::create(['name' => 'user']);
        $user->givePermissionTo('manage records'); 
          
        $user1 = \App\Models\User::factory()->create([
            'name' => 'Admin',
            'email' => 'admin@admin.com',
            'password' => bcrypt('admin'),
        ]);       
        $user1->assignRole($admin); 

        $user2 = \App\Models\User::factory()->create([
            'name' => 'User',
            'email' => 'user@user.com',
            'password' => bcrypt('user'),
        ]);
        $user2->assignRole($user); 
    }
}
