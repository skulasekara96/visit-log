<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
})->name("home");

Route::get('/search', function () {
    return view('search');
})->middleware('auth', 'CheckPermission:manage records')->name("search");

Route::get('/visitor_list', function () {
    return view('visitor_list');
})->middleware('auth', 'CheckPermission:manage records')->name("visitor_list");

Route::get('/visit_log', function () {
    return view('visit_log');
})->middleware('auth', 'CheckPermission:manage records')->name("visit_log");

Route::get('/user_list', function () {
    return view('user_list');
})->middleware('auth', 'CheckPermission:manage records')->name("user_list");

Route::middleware(['auth:sanctum', 'verified', 'CheckPermission:manage records'])->get('/dashboard', function () {
    return view('search');
})->name('dashboard');
