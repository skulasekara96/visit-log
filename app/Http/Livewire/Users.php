<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Models\User;
use Illuminate\Support\Facades\Auth;

class Users extends Component
{
    public $page = 1;
    public $res = 10;
    public $list_size = 0;

    public $msg = null;
    protected $listeners = ['role_update' => 'role_update', 'delete'=>'delete'];
    public function render()
    {
        $user = Auth::user();
        //permission check
        if(!$user->hasPermissionTo('manage users')){
            return abort(404);
        }
        //-----------------  */
        $qu = User::query()
                        ->select('*')
                        ->offset(((int) $this->page - 1)* (int) $this->res)
                        ->limit($this->res)
                        ->orderBy('id', 'desc');
        $users = $qu->get();   

        $user_list = array();
        //load user list to var ith its role
        foreach ($users as $user){
            $user_data = $user->toArray();
            $role = @$user->getRoleNames()->toArray()[0];
            $user_data['role'] = $role;
            
            array_push($user_list,$user_data);
            
        }             
        return view('livewire.users', compact('user_list'));

    }
    public function role_update($role, $id){
        $user = Auth::user();
        //permission check
        if(!$user->hasPermissionTo('manage users')){
            return abort(404);
        }
        // ---------------- */
        $user = User::find($id);
        //remove assigned role
        if($user){
            if($role == 'admin' || $role == 'user'){
                $user->syncRoles([$role]);
            }else{
                $user->syncRoles([]);
            }
            
            $this->msg = " Role changed to ".$role;
        }else{
            $this->msg = "Invalid Data";
        }
        
        
    }
    public function delete($id){
        $user = Auth::user();
        //permission check
        if(!$user->hasPermissionTo('manage users')){
            return abort(404);
        }
        // ---------------- */
        $user = User::find($id);
        $this->msg = "User Deleted";
        $user->syncRoles([]);
        $user->delete();

    }
}
