<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Models\visit_records;

class Vlog extends Component
{
    public $page = 1;
    public $res = 10;
    public $list_size = 0;

    public $from = null;
    public $to = null;
    public $i_id = null;
    public $direction = "all";
    public $temp_from = null;
    public $temp_to = null;
    protected $listeners = ['select_id' => 'select_id'];
    
    public function render()
    {
        $qu = visit_records::query()
                        ->select('visit_records.*', 'individuals.name')
                        ->offset(((int) $this->page - 1)* (int) $this->res)
                        ->limit($this->res)
                        ->join('individuals', 'visit_records.visitor_id', '=', 'individuals.id')
                        ->orderBy('id', 'desc');
                        
        if($this->i_id != null){
            $qu->where('visit_records.visitor_id', $this->i_id);
        } 
        if($this->from != null){
            $qu->where('visit_records.created_at', '>', date($this->from));
        } 
        if($this->to != null){
            $qu->where('visit_records.created_at', '<', date($this->to));
        } 
        if($this->direction != 'all'){
            $qu->where('visit_records.direction', $this->direction);
        } 
        if($this->temp_from != null){
            $qu->where('visit_records.temp', '>=', $this->temp_from);
        } 
        if($this->temp_to != null){
            $qu->where('visit_records.temp', '<=', $this->temp_to);
        } 

        $log = $qu->get()->toArray();
        $this->list_size = sizeof($log);
        return view('livewire.vlog', compact('log'));

    }
    public function next(){
        if($this->list_size == $this->res){
            $this->page++;
        }
    }    
    public function back(){
        if($this->page > 1){
           $this->page--; 
        }        
    }
    public function select_id($id){        
        $this->i_id = $id;
    }
}
