<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Models\individuals;

class Vlist extends Component
{
    public $page = 1;
    public $res = 10;
    public $list_size = 0;

    public $index = null;
    public $phone = null;
    public $nic = null;
    public $name = null;
    public $type = 'all';
    public $allowed = 2;
    public $gender = 'all';
    public $age = null;
    public $address = null;
    public $info = null;
    public $i_id = null;

    protected $listeners = ['select_id' => 'select_id'];

    public function render()
    {
        
        $qu = individuals::query()
                        ->select('*')
                        ->offset(((int) $this->page - 1)* (int) $this->res)
                        ->limit($this->res)
                        ->orderBy('id', 'desc');
        if($this->name != null){
            $qu->where('name', 'like', '%'.$this->name.'%');
        } 
        if($this->index != null){
            $qu->where('index', $this->index );
        } 
        if($this->i_id != null){
            $qu->where('id', $this->i_id );
        } 
        if($this->phone != null){
            $qu->where('phone', 'like', '%'.$this->phone.'%' );
        } 
        if($this->nic != null){
            $qu->where('nic', $this->nic );
        } 
        if($this->type != 'all'){
            $qu->where('type', $this->type );
        } 
        if($this->age != null){
            $qu->where('age', $this->age );
        } 
        if($this->gender != 'all'){
            $qu->where('gender', $this->gender );
        } 
        if($this->address != null){
            $qu->where('address', 'like', '%'.$this->address.'%' );
        } 
        if($this->info != null){
            $qu->where('info', 'like', '%'.$this->info.'%' );
        } 
        if($this->allowed != '2'){
            $qu->where('allowed', 'like', '%'.$this->allowed.'%' );
        } 
        $list = $qu->get()->toArray();
        $this->list_size = sizeof($list);
        return view('livewire.vlist', compact('list'));
    }
    public function next(){
        if($this->list_size == $this->res){
            $this->page++;
        }
    }    
    public function back(){
        if($this->page > 1){
           $this->page--; 
        }        
    }
    public function select_id($id){        
        $this->i_id = $id;
    }
}
