<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Models\individuals;
use App\Models\visit_records;
use Illuminate\Support\Facades\Auth;

class Search extends Component
{
    public $results ="";
    public $error = null;
    public $squ = "";
    public $data_type = "";
    public $data = null;

    public $i_id = null;
    public $index = null;
    public $phone = null;
    public $nic = null;
    public $name = null;
    public $type = 'unknown';
    public $allowed = '1';
    public $gender = 'unknown';
    public $age = null;
    public $address = null;
    public $info = null;
    public $reg_date = null;

    public $temp = null;
    public $direction = null;

    protected $listeners = ['select_id' => 'select_id'];

    protected $rules = [
        'index' => 'numeric|nullable',
        'phone' => 'numeric|nullable',
        'nic' => 'string|nullable',
        'name' => 'string|required',
        'type' => 'string|required',
        'allowed' => 'numeric|required',
        'gender' => 'string|required',
        'age' => 'numeric|nullable',
        'address' => 'string|nullable',
        'info' => 'string|nullable',
    ];
    public function search(){
        
    }
    
    public function render()
    {
        $this->data = null;
        $this->reset(['i_id', 'index' ,'phone', 'nic', 'name', 'type', 'allowed', 'gender', 'age', 'address', 'info']);
        if (preg_match("/^[0-9]{10}$/i", $this->squ) > 0 ) {
            $this->sata_type = "phone";
            $info = individuals::where('phone', $this->squ);
            if($info->count() >0){
                $this->data =$info->first();                                
                $this->results = "Match Found ";
            }else{
                $this->results = "Phone not Found - Register Now";
                $this->phone = $this->squ;
            }
        }else if (preg_match("/^[0-9]{9}[vx]$/i", $this->squ) > 0 || preg_match("/^[0-9]{11}$/i", $this->squ) > 0  ) {
            $this->data_type = "nic";
            $info = individuals::where('nic', $this->squ);
            if($info->count() >0){
                $this->data =$info->first();                                
                $this->results = "Match Found ";
            }else{
                $this->results = "NIC not Found - Register Now";
                $this->nic = $this->squ;
            }
        }else if(preg_match("/^[0-9]{1,}$/i", $this->squ) > 0){
            $this->data_type = "index";
            $info = individuals::where('index', $this->squ);
            if($info->count() >0){
                $this->data =$info->first();                                
                $this->results = "Match Found ";
            }else{
                $this->results = "index not Found - Register Now";
                $this->index = $this->squ;
            }
        }else{
            $this->data_type = "serach";
            //$this->reset(['index' ,'phone', 'nic', 'name', 'type', 'allowed', 'gender', 'age', 'address', 'info', 'results']);
        }
        //displaying error
        if($this->error != null){
            $this->results = $this->error; 
            $this->error = null;
        }
        if($this->data != null){
            $this->i_id = $this->data->id;
            $this->index = $this->data->index;
            $this->phone = $this->data->phone;
            $this->nic = $this->data->nic;
            $this->name = $this->data->name;
            $this->type = $this->data->type;
            $this->allowed = $this->data->allowed;
            $this->gender = $this->data->gender;
            $this->age = $this->data->age;
            $this->address = $this->data->address;
            $this->info = $this->data->info;
        }else{
            
        }
        return view('livewire.search');        
    }
    public function add_new(){
        $validatedData = $this->validate();
        //validate custom
        $id_count = 0;        

        if($this->index == "" ){

        }else {
            if(preg_match("/^[0-9]{1,}$/i", $this->index) > 0 ){
                //index is set and valid
                $info = individuals::where('index', $this->index);
                if($info->count() >0){
                    //index already in use
                    $this->results = "Index already exists";
                    $this->squ= $this->index;
                    $this->index = null;
                    return;
                }
                $id_count++;
    
            }else {
                $this->index = "";
            }
        }
        if ($this->phone == "") {
            
        }else {
            if(preg_match("/^[0-9]{10}$/i", $this->phone) > 0 ){
                //phone is set and valid
                $info = individuals::where('phone', $this->phone);
                if($info->count() >0){
                    //index already in use
                    $this->results = "Phone already exists";
                    $this->squ= $this->phone;
                    $this->phone = null;
                    return;
                }
                $id_count++;
            }else {
                $this->phone = "";
            }
        }
        if ($this->nic == "") {
            
        }else {
            if(preg_match("/^[0-9]{9}[vx]$/i", $this->nic) > 0 || preg_match("/^[0-9]{11}$/i", $this->nic) > 0){
                //nic is set and valid
                $info = individuals::where('nic', $this->nic);
                if($info->count() >0){
                    //index already in use
                    $this->results = "NIC already exists";
                    $this->squ= $this->nic;
                    $this->nic = null;
                    return;
                }
                $id_count++;                
            }else {
                $this->nic = "";
            }
        }   
        if ($id_count < 1) {
            $this->results = "Valid Index numbe or Phone Number or NIC required";
            return;
        }
                
        individuals::create($validatedData);
        $this->results = "Visitor Registered";
        
    }
    public function update(){         
        if ($this->i_id == null) {
            $this->error = "Search and select visitor first";
            return;
        }
        $validatedData = $this->validate();
        //validate custom
        
        $id_count = 0; 
        if($this->index == "" ){

        }else {
            if(preg_match("/^[0-9]{1,}$/i", $this->index) > 0 ){
                //index is set and valid
                $this->error = "1";
                $info = individuals::where('index', $this->index);
                if($info->count() >0){
                    $data=$info->first();
                    if ($data->id != $this->i_id) {
                        $this->error = "Index already in use";
                        return;
                    } 
                }
                $id_count++;    
            }else {
                $this->index = "";
            }
        }
        if ($this->phone == "") {
            
        }else {
            if(preg_match("/^[0-9]{9,10}$/i", $this->phone) > 0 ){
                //phone is set and valid
                $info = individuals::where('phone', $this->phone);
                if($info->count() >0){
                    $data=$info->first();
                    if ($data->id != $this->i_id) {
                        $this->error = "Phone already in use";
                        return;
                    } 
                }
                $id_count++;
            }else {
                $this->phone = "";
            }
        }
        if ($this->nic == "") {
            
        }else {
            if(preg_match("/^[0-9]{9}[vx]$/i", $this->nic) > 0 || preg_match("/^[0-9]{11}$/i", $this->nic) > 0){
                //nic is set and valid
                $info = individuals::where('nic', $this->nic);
                if($info->count() >0){
                    $data=$info->first();
                    if ($data->id != $this->i_id) {
                        $this->error = "NIC already in use";
                        return;
                    } 
                }
                $id_count++;                
            }else {
                $this->nic = "";
            }
        }   
        if ($id_count < 1) {
            $this->error = "Valid Index numbe or Phone Number or NIC required";
            return;
        } 
        if($validatedData['index'] == ""){
            $validatedData['index'] = null;
            //$this->results = "Valid Index numbe or Phone Number or NIC required";
            //return;
        }          
        individuals::find($this->i_id)->update($validatedData);
        $this->error = "Visitor Updated";
        
    }
    public function record($direction){
        if ($this->i_id == null) {
            $this->error = "Search and select visitor first";
            return;
        }
        //$this->error = $this->i_id; 
        //return;
        $record = array(    "temp" => $this->temp,
                            "user_id" => Auth::id(),
                            "visitor_id" => $this->i_id,
                            "direction" => $direction);
        visit_records::create($record);
        $this->error = "Record Added";        
        $this->squ = "";
        if($this->allowed == 1 && $this->i_id != null){
            $this->dispatchBrowserEvent('focus-el', ['element' => "squ"]);
        }
    }

    public function select_id($id){        
        $data = individuals::find($id);
        if($data){
            if($data['nic'] != ""){
               $this->squ = $data['nic']; 
            }else if($data['phone'] != ""){
               $this->squ = "0".$data['phone']; 
            }else if($data['index'] != ""){
               $this->squ = $data['index']; 
             }
            
        }

    }  
    
}
