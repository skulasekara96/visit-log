<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class individuals extends Model
{
    use HasFactory;
    protected $fillable = [
        'index',
        'phone', 
        'nic', 
        'name',
        'type',
        'allowed',
        'gender',
        'age',
        'address',
        'info',
    ];
}
