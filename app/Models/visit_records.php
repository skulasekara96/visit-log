<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class visit_records extends Model
{
    use HasFactory;
    protected $fillable = [
        'temp', 
        'user_id', 
        'direction',
        'visitor_id',
    ];
}
