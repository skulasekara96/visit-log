<div>
    <table class="table">
        <tr>
            <th>Name</th>
            <th>Index</th>
            <th>Phone</th>
            <th>NIC</th>
            <th>Type</th>
            <th>Age</th>
            <th>Gender</th>
            <th>Address</th>
            <th>info</th>
            <th>State</th>
        </tr>
        <tr>
            <td>
                <input class="form-control " type="text" id="name" wire:model="name">
            </td>
            <td>
                <input class="form-control " type="number" id="index" wire:model="index">
            </td>
            <td>
                <input class="form-control " type="number" id="phone" wire:model="phone">
            </td>
            <td>
                <input class="form-control " type="text" id="nic" wire:model="nic">
            </td>
            <td>
                <select name="type" id="type" class="form-control inline-flex" wire:model="type">
                    <option value="all">All</option>
                    <option value="unknown">Unknown</option>
                    <option value="visitor">Visitor</option>
                    <option value="employee">Employee</option>
                </select>                
            </td>
            <td>
                <input class="form-control " type="number" id="age" wire:model="age">
            </td>
            <td>
                <select name="gender" id="gender" class="form-control inline-flex" wire:model="gender">
                    <option value="all">All</option>
                    <option value="unknown">Unknown</option>
                    <option value="male">Male</option>
                    <option value="female">Female</option>
                </select>
            </td>
            <td>
                <input class="form-control " type="text" id="address" wire:model="address">
            </td>
            <td>
                <input class="form-control " type="text" id="info" wire:model="info">
            </td>
            <td>
                <select name="allowed" id="allowed" class="form-control inline-flex" wire:model="allowed">
                    <option value="2">All</option>
                    <option value="1">Access Allowed</option>
                    <option value="0">Access Denied</option>
                </select>
            </td>
        </tr>        
        @foreach($list as $visitor)
            <tr>
                <td><a class="btn btn-primary" href="{{route('search')}}?id={{ $visitor['id']}}" >{{ $visitor['name']}}</a></td>
                <td>{{ $visitor['index']}}</td>
                <td>{{ $visitor['phone']}}</td>
                <td>{{ $visitor['nic']}}</td>
                <td>{{ $visitor['type']}}</td>
                <td>{{ $visitor['age']}}</td>
                <td>{{ $visitor['gender']}}</td>
                <td>{{ $visitor['address']}}</td>
                <td>{{ $visitor['info']}}</td>
                <td>{{ $visitor['allowed']}}</td>
            </tr>            
        @endforeach
        <tr>
            <td colspan="10">
                <div class="input-group  m-auto" style="width:max-content">
                    <div class="input-group-prepend">
                        <button class="btn btn-primary " wire:click="back"><< Back</button>
                        <span class="input-group-text" >Page:</span>
                        
                    </div>
                    <input class="form-control" type="number" id="page"  style="width:20px;" wire:model="page">
                    <div class="input-group-prepend">
                        <span class="input-group-text" >Result:</span>                                            
                    </div>
                    <input class="form-control" type="number" id="res"  wire:model="res">
                    <div class="input-group-append">
                    <button class="btn btn-primary " wire:click="next">Next >></button>
                    </div>
                </div>
            </td>
        </tr>
        
    </table>
</div>
