<div>
    <p class=" text-danger">{{$msg}}</p>
    <table class=" table" style="max-width:700px; margin:auto;">
        <tr>
            <th>Name</th>
            <th>e-mail</th>
            <th>Role</th>
            <th></th>
        </tr>
        @foreach($user_list as $user)
            <tr>
                <td>{{ $user['name'] }}</td>
                <td>{{ $user['email'] }}</td>
                <td>
                    <?php
                        if($user['role'] == "admin" || $user['role'] == "user"){
                            $user[$user['role']] = "selected";
                        }else{
                            $user['none'] = "selected";
                        }
                    ?>
                    <select name="" id="" onchange="update_role(this.value,{{$user['id']}})">
                        <option value="none" {{ @$user['none'] }}>None</option>
                        <option value="user" {{ @$user['user'] }}>User</option>
                        <option value="admin" {{ @$user['admin'] }}>Admin</option>
                    </select>                    
                </td>
                <td>
                    <button class=" btn btn-danger" onclick="delete_user({{ $user['id'] }})">Delete</button>
                </td>
            </tr>
        @endforeach
    </table>
    <script>
        function update_role(new_role, u_id){
            if(confirm("Do you want to chang role to: "+new_role)){
                console.log('Role: '+new_role+"| ID :"+u_id);  
                Livewire.emit('role_update', new_role, u_id );
            } 
        };        
        function delete_user(u_id){
            if(confirm("Do you Really want to delete this user ?")){
                Livewire.emit('delete', u_id );
            }           
        } ;
    </script>
</div>
