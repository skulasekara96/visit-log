<div>    
    <div class="input-group mb-3" style="max-width: 500px;   margin: auto;">
        <input type="text" wire:model.defer="squ" wire:keydown.enter="search" id="squ" class="form-control" placeholder="Search with Phone No, NIC or Index "  aria-label="search" aria-describedby="search" autofocus>
        <div class="input-group-append">
            <button wire:click="search" class="btn btn-outline-secondary" type="button">Search</button>
        </div>
    </div>
    <hr>
        
    <br>
    @if($allowed == 1 && $i_id != null)
    <div class=" container"  style="max-width:500px;">
        <div class="row">
            <div class="col-12">
                <h3 class=" h4">Record 
                        @if($gender == "male")
                            Mr.
                        @elseif($gender == "female")
                            Mrs.
                        @endif
                        {{ ucfirst($name)}} Going IN or OUT</h3>
            </div>
        </div>
        <div class="row">
            <div class="col-4">
                <label class="mt-1" for="temp"><b> Temperature</b></label>
            </div>
            <div class="col-8">
                <input class=" form-control" type="number" id="temp"  wire:model.defer="temp" wire:keydown.+="record('in')" wire:keydown.-="record('out')" autofocus >
            </div>
        </div>            
        <div class="row mt-1 mb-3">
            <div class="col-6 ">
                <button class=" btn btn-primary w-100" wire:click="record('in')">IN [+]</button>
            </div>
            <div class="col-6">
                <button class=" btn btn-primary w-100" wire:click="record('out')">OUT [-]</button>
            </div>
        </div>            
        <div class="row mt-1 mb-3">
            <div class="col-12 ">
                <a href="{{ route('visit_log')}}?id={{$i_id}}" class=" btn btn-secondary w-100" >Visit Log</a>
            </div>
        </div>            
    </div>
    <hr>     
    @endif
    <div class="container rounded filled" style="max-width:500px;">
    <div class="row mt-2">
            <div class="col-12">
                <h3 class=" h4">
                @if($data == null)
                Add Visitor 
                @else
                Update Visitor
                @endif  
                Information
                </h3>
            </div>
        </div>
        <div class="row mb-1">
            <div class="col text-danger text-center">
                <p>{{ $results }}</p>
            </div>
        </div>
        <div class="row mb-1 form-group">
            <div class=" col-4">
                <label class="mt-1" for="index"><b>Index number</b></label>                
            </div>
            <div class=" col-8">
                <input class="form-control inline-flex" type="number" id="index" wire:model.defer="index" >
                @error('index') <span class="error text-danger">{{ $message }}</span> @enderror
            </div>
        </div>
        <div class="row mb-1 form-group">
            <div class=" col-4">
                <label class="mt-1" for="phone"><b>Phone number</b></label>                
            </div>
            <div class=" col-8">
                <input class="form-control inline-flex" type="text" id="phone" wire:model.defer="phone">
                @error('phone') <span class="error text-danger">{{ $message }}</span> @enderror
            </div>
        </div>
        <div class="row mb-1 form-group">
            <div class=" col-4">
                <label class="mt-1" for="nic"><b>NIC number</b></label>                
            </div>
            <div class=" col-8">
                <input class="form-control inline-flex" type="text" id="nic" wire:model.defer="nic">
                @error('nic') <span class="error text-danger">{{ $message }}</span> @enderror
            </div>
        </div>
        <div class="row mb-1 form-group">
            <div class=" col-4">
                <label class="mt-1" for="name"><b>Name</b></label>                
            </div>
            <div class=" col-8">
                <input class="form-control inline-flex" type="text" id="name" wire:model.defer="name">
                @error('name') <span class="error text-danger">{{ $message }}</span> @enderror
            </div>
        </div>
        <div class="row mb-1 form-group">
            <div class=" col-4">
                <label class="mt-1" for="type"><b>Type</b></label>                
            </div>
            <div class=" col-8">
                <select name="type" id="type" class="form-control inline-flex" wire:model.defer="type">
                    <option value="unknown">Unknown</option>
                    <option value="visitor">Visitor</option>
                    <option value="employee">Employee</option>
                </select>
            </div>
        </div>
        <div class="row mb-1 form-group">
            <div class=" col-4">
                <label class="mt-1" for="allowed"><b>Allowed</b></label>                
            </div>
            <div class=" col-8">
                <select name="allowed" id="allowed" class="form-control inline-flex" wire:model.defer="allowed">
                    <option value="1">Access Allowed</option>
                    <option value="0">Access Denied</option>
                </select>
            </div>
        </div>
        <div class="row mb-1 form-group">
            <div class=" col-4">
                <label class="mt-1" for="gender"><b>Gender</b></label>                
            </div>
            <div class=" col-8">
                <select name="gender" id="gender" class="form-control inline-flex" wire:model.defer="gender">
                    <option value="unknown">Unknown</option>
                    <option value="male">Male</option>
                    <option value="female">Female</option>
                </select>
            </div>
        </div>
        <div class="row mb-1 form-group">
            <div class=" col-4">
                <label class="mt-1" for="age"><b>Age</b></label>                
            </div>
            <div class=" col-8">
                <input class="form-control inline-flex" type="number" id="age" wire:model.defer="age">
                @error('age') <span class="error text-danger">{{ $message }}</span> @enderror
            </div>
        </div>
        <div class="row mb-1 form-group">
            <div class=" col-4">
                <label class="mt-1" for="address"><b>Address</b></label>                
            </div>
            <div class=" col-8">
                <textarea class="form-control" name="address" id="address" cols="30" rows="3" wire:model.defer="address"></textarea>
                @error('address') <span class="error text-danger">{{ $message }}</span> @enderror
            </div>
        </div>
        <div class="row mb-1 form-group">
            <div class=" col-4">
                <label class="mt-1" for="info"><b>Info</b></label>                
            </div>
            <div class=" col-8">
                <textarea class="form-control" name="info" id="info" cols="30" rows="3" wire:model.defer="info"></textarea>
                @error('info') <span class="error text-danger">{{ $message }}</span> @enderror
            </div>
        </div>
        <div class="row mb-1 form-group">
            <div class=" col-4">           
            </div>
            <div class=" col-8">
                @if($data == null)
                <button class=" btn btn-primary w-100" wire:click="add_new">ADD</button>
                @else
                <button class=" btn btn-primary w-100" wire:click="update">UPDATE</button>
                @endif       
            </div>
        </div>
    </div>
    <style>
        @if($allowed == 0)
            .filled{
                background-color: #e3342fab;
                max-width: 500px;
                padding: 2px 15px;
            }
        @endif        
    </style>

    @if($allowed == 1 && $i_id != null)
        <script>
            document.getElementById("temp").focus();
            console.log("Focus Temp");        
        </script>
    @endif
    <script>
        window.addEventListener('focus-el', event => {
            document.getElementById(event.detail.element).focus();
        })
    </script>
    {{-- $data_type --}}{{-- $squ --}}{{--$this->getErrorBag()--}}
    <hr>
    <br>
</div>
