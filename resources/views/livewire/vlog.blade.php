<div>
    <table class=" table w-100" style="max-width: 650px; margin: auto;">
        <tr>
            <th>Date and time</th>
            <th>Visitor</th>
            <th>Direction</th>
            <th>Temprature</th>
        </tr>
        <tr>
            <th>
            From: <input wire:model="from" class="form-control " style=" max-width: 200px;" type="text" >
            To:<input wire:model="to" class="form-control "  style=" max-width: 200px;" type="text" >
            </th>
            <th>
              Visitor ID
             <input type="number" wire:model="i_id">
            </th>
            <th>
                <br>
                <select class="form-control " wire:model="direction">
                    <option value="all">All</option>
                    <option value="in">IN</option>
                    <option value="out">OUT</option>
                </select>
            </th>
            <th>
                From:
                <input type="number" wire:model="temp_from" style="    max-width: 90px;" class=" form-control">
                To:
                <input type="number" wire:model="temp_to" style="    max-width: 90px;" class=" form-control">
            </th>
        </tr>
        @foreach($log as $entry)
            <?php 
            $date = preg_replace("/.{5}$/i", "",$entry['created_at']);
            $date = preg_replace("/T/i", " ", $date);
            ?>
            <tr>
                <td>{{ $date }}</td>
                <td>{{$entry['name']}}</td>
                <td>{{$entry['direction']}}</td>
                <td>{{$entry['temp']}}</td>
            </tr>
        @endforeach    
        <tr>
            <td colspan="4">
                <div class="input-group  m-auto" style="width:max-content">
                    <div class="input-group-prepend">
                        <button class="btn btn-primary " wire:click="back"><< Back</button>
                        <span class="input-group-text" >Page:</span>
                        
                    </div>
                    <input class="form-control" type="number" id="page"  style="width:20px;" wire:model="page">
                    <div class="input-group-prepend">
                        <span class="input-group-text" >Result:</span>                                            
                    </div>
                    <input class="form-control" type="number" id="res"  wire:model="res">
                    <div class="input-group-append">
                    <button class="btn btn-primary " wire:click="next">Next >></button>
                    </div>
                </div>
            </td>
        </tr>    
    </table>
</div>
