<x-app-layout>
    <x-slot name="header">
    </x-slot>

    <div class="container">
        <div class="row" style=" min-height: 50vh;">
            <div class="col" style="overflow-x: scroll;">            
                <livewire:users />
            </div>
        </div>
        <div class="row">
            <div class="col w-100 text-center">
                <p>Developed By Shashi Web MAX</p>
                <a href="mailto:shashi@nittambuwa.net">shashi@nittambuwa.net</a>

            </div>
        </div>
    </div> 
    <script>
        window.onload = function() {
            <?php 
            if (isset($_GET['id'])) {
                echo "Livewire.emit('select_id', ".$_GET['id'].");";
            }
            ?>
        };
    </script>
</x-app-layout>
